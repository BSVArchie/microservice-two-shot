from django.db import models
from django.urls import reverse

# Create your models here.

class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=200)


class Hats(models.Model):
    name = models.CharField(max_length=200)
    fabric = models.TextField()
    style = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    picture = models.URLField(null=True)

    location = models.ForeignKey(
        LocationVO,
        related_name="location",
        on_delete=models.CASCADE, null=True
    )

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("api_show_ hat", kwargs={"pk": self.pk})
