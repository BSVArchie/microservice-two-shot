from django.contrib import admin
from .models import Hats, LocationVO

# Register your models here.
@admin.register(Hats)
class HatsAdmin(admin.ModelAdmin):
    pass


@admin.register(LocationVO)
class LocationVO(admin.ModelAdmin):
    list_display = [
        "id",
        'import_href',
        'closet_name',
    ]
