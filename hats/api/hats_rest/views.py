from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .models import Hats, LocationVO
from common.json import ModelEncoder

# Create your views here.



class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = ["import_href", "closet_name",]




class HatDetailEncoder(ModelEncoder):
    model = Hats
    properties = [
        "name",
        "fabric",
        "style",
        "color",
        "picture",
        "location",
        ]
    encoders = {
        "location": LocationVOEncoder(),
    }




class HatsListEncoder(ModelEncoder):
    model = Hats
    properties = ["name", "id", "picture"]

    def get_extra_data(self, o):
        return {"location": o.location.closet_name}






@require_http_methods(["GET", "POST"])
def api_list_hats(request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id is not None:
            hats = Hats.objects.filter(location=location_vo_id)
            print(hats)
            return JsonResponse(
                {"hats": hats},
                encoder=HatsListEncoder
            )
        else:
            hats = Hats.objects.all()
            return JsonResponse(
                {"hats": hats},
                encoder=HatsListEncoder
            )
    else:
        content = json.loads(request.body)
        print(content)
        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id (should be href)"},
                status=400
            )
        hat = Hats.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False
        )






@require_http_methods(["GET", "DELETE"])
def api_show_hat(request, pk):
    if request.method == "GET":
        hat = Hats.objects.get(id=pk)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False
        )
    else:
        count, _ = Hats.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
