import { useState, useEffect } from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatsList from './HatsList';
import HatDetails from './HatDetails';
import HatForm from './HatForm';


function App() {
  const [hats, setHats] = useState([]);

  async function getHats() {
    const response = await fetch ('http://localhost:8090/api/hats/');
    if (response.ok) {
      const { hats } = await response.json();
      setHats(hats);
    }
  }

  useEffect(() => {
    getHats();
  }, []);

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/hats" element={<HatsList hats={hats} getHats={getHats} />} />
          <Route path="/hats/:id" element={<HatDetails hats={hats} />} />
          <Route path="/hats/new" element={<HatForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
