import { NavLink } from 'react-router-dom';
// import { useState, useEffect } from 'react';

function HatsList({ hats, getHats }) {

    // const [hats, setHats] = useState([]);

    // async function getHats() {
    //     const response = await fetch ('http://localhost:8090/api/hats/');
    //     if (response.ok) {
    //     const hats = await response.json();
    //     setHats(hats);
    //     }
    // }

    // useEffect(() => {
    //     getHats();
    // }, []);

    const deleteHat = async(id) => {
        const deleteUrl = `http://localhost:8090/api/hats/${id}/`
        const fetchConfig = {
            method: "delete",
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(deleteUrl, fetchConfig);
        if (response.ok) {
            const didItDelete = await response.json();
            console.log(didItDelete);
            getHats()

        }
    }

    return (
        <div>
            <button className="btn btn-dark">
                <NavLink className="nav-link" to="/hats/new">Add a Hat</NavLink>
            </button>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Hat</th>
                        <th>Location</th>
                        <th>Hat Details</th>
                    </tr>
                </thead>
                <tbody>
                    {hats.map( hat => {
                        console.log(hat)
                        return (
                            <tr key={hat.id}>
                                <td>{hat.name}</td>
                                <td>{hat.location}</td>
                                <td>
                                    <img src={`${hat.picture}`} className="img-thumbnail .05rem" />
                                </td>
                                <td className="nav-item">
                                    <NavLink className="nav-link" to={`/hats/${hat.id}/`}>  Hats</NavLink>
                                </td>
                                <td>
                                    <button type="button" id={hat.id} onClick={() => deleteHat(hat.id)} className="btn btn-danger">Remove Hat</button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default HatsList;
