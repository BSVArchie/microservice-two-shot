import { useParams } from "react-router-dom";
import { useState, useEffect } from 'react';

function HatDetails() {
    const [hat, setHat] = useState({})
    const params = useParams()
    // console.log(hat)

    const getHat = async () => {
        const response = await fetch (`http://localhost:8090/api/hats/${params.id}/`);
        if (response.ok) {
          const data = await response.json();
          console.log(data)
          setHat(data);
        }
      }

    useEffect(()=> {
        getHat();
      }, []);
    if (hat.location === undefined){
        return null}
    else {
        return (
            <div className="container align-middle">
                <div className="card" style={{width: "18rem", margin: "10px"}}>
                    <div className="card-body">
                        <h5 className="card-title">{hat.name}</h5>
                        <img src={hat.picture} className="card-img-top" alt="..."/>
                        <p className="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    </div>
                    <ul className="list-group list-group-flush">
                        <li className="list-group-item">Closet: {hat.location.closet_name}</li>
                        {/* <li className="list-group-item">A second item</li>
                        <li className="list-group-item">A third item</li> */}
                    </ul>
                    </div>
            </div>
        );
    }
}


export default HatDetails;




// <div className="container">
//         <h2>Upcoming conferences</h2>
//         <div className="container">
//           {hats.map((hat, id) => {
//             return (
//               <getHat key={id} hat={hats.name} />
//             );
//           })}
//         </div>
