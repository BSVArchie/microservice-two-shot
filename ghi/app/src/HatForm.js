import React, { useEffect, useState } from 'react';


function HatForm () {
    const [location, setLocation] = useState([]);

    const [name, setName] = useState('');
    const [fabric, setFabric] = useState('');
    const [style, setStyle] = useState('');
    const [picture, setPicture] = useState('');
    const [href, setHref] = useState('');


    const handleNameChange = (event) => {
      const value = event.target.value;
      setName(value);
    };

    const handleFabricChange = (event) => {
      const value = event.target.value;
      setFabric(value);
    };

    const handleStyleChange = (event) => {
      const value = event.target.value;
      setStyle(value);
    };

    const handlePictureChange = (event) => {
      const value = event.target.value;
      setPicture(value);
    };

    const handleHrefChange = (event) => {
      const value = event.target.value;
      setHref(value);
    };


    const fetchData = async () => {
        const url = 'http://localhost:8100/api/locations/';

        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            // console.log(data)
            setLocation(data.locations)
        }

    }

    useEffect(() => {
      fetchData();
    }, []);


    const handleSubmit = async (event) => {
      event.preventDefault();

      const data = {};

      data.name = name;
      data.fabric = fabric;
      data.style = style;
      data.picture = picture;
      data.location = href;
      console.log(data)

      const hatUrl = 'http://localhost:8090/api/hats/';
      const fetchOptions = {
        method: 'post',
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
        },
      };

      const response = await fetch(hatUrl, fetchOptions);
      if (response.ok) {
        const newHat = await response.json();
        console.log(newHat)

        setName('');
        setFabric('');
        setStyle('');
        setPicture('');
        setHref('');
      };
    }


    return (
        <div className="container">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>New Hat</h1>
              <form onSubmit={handleSubmit} id="create-conference-form">
                <div className="form-floating mb-3">
                  <input onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" value={name} />
                  <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleFabricChange} placeholder="fabric" required type="text" name="fabric" id="fabric" className="form-control" value={fabric} />
                  <label htmlFor="start">Fabric</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleStyleChange} placeholder="style" required type="text" name="style" id="style" className="form-control" value={style} />
                  <label htmlFor="ends">Style</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handlePictureChange} placeholder="Image URL" required type="text" name="picture" id="picture" className="form-control" value={picture} />
                  <label htmlFor="ends">Image URL</label>
                </div>
                <div className="mb-3">
                  <select required onChange={handleHrefChange} name="closet" id="closet" className="form-select">
                    <option value="">Closet Name</option>
                    {location.map(closet => {
                      return (
                        <option key={closet.id} value={closet.href}>
                          {closet.closet_name}
                        </option>
                      )
                    })}
                  </select>
                  </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      </div>
      );
}

export default HatForm
