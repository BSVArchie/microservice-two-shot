# Wardrobify

Team:

* Harut Sarkisyan - Shoes microservice?
* Matthew Archbold - Hats microservice?

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

## Hats microservice

# Django
- Create a model for all the hat properties needed (fabric, style_name, color, URL picture, the location in wordrobe) - Migrate in docker exec
- Write a views.py function that get a list of hats from the db and return a JSON encoded response
    - ensure urls are correct (test in insomnia)
- Write a views.py function that takes in  JSON and Creates a hat in the db
    - ensure urls are correct (test in insomnia)
- Write a views.py function that takes in  JSON and Deletes a hat in the db
    - ensure urls are correct (test in insomnia)

- create poll for the wardrobe location data (VO must be unique id probably href???)

# React
- Create js files with components for listHats and details, createHat, deleteHat

- ensure all the links/navigation work with components

-reduct store with fetches?????


Explain your models and integration with the wardrobe
microservice, here.
